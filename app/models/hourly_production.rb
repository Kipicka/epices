class HourlyProduction < ApplicationRecord
  belongs_to :inverter

  validates :date,       :presence => true

  validates  :hour,       :presence => true

  validates  :inverter,   :presence => true
  validates_associated :inverter

  validates  :production, :presence => true, :numericality => { greater_than_or_equal_to: 0 }

  def hour
    self[:hour].strftime("%k")
  end

  def date
    self[:date].strftime("%d/%m/%y")
  end

end
