class Inverter < ApplicationRecord
  has_many :hourly_production

  validates :id, :presence => true, :uniqueness => true

end
