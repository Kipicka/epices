class HourlyProductionsController < ApplicationController
  def index
    #get all dates in database
    @dates = HourlyProduction.select(:date).map(&:date).uniq.sort
    @hourly_productions = []

    unless @dates.empty?
      #use default date if no param specified
      default_date =  @dates.first
      #if invalid param, get default date
      @date = params[:date] || default_date

      begin
        @hourly_productions = HourlyProduction.where(date: Date.strptime(@date, "%d/%m/%y"))
        @hourly_productions = HourlyProduction.where(date: Date.strptime(default_date, "%d/%m/%y")) if @hourly_productions.empty?
      rescue
        @hourly_productions = HourlyProduction.where(date: Date.strptime(default_date, "%d/%m/%y"))
      end
    end
    #get sum for the day and for each inverter
    if @hourly_productions.any?
      @day_production = @hourly_productions.sum(:production)
      @production_by_inverter = @hourly_productions.group(:inverter_id).sum(:production)
    end
  end

  def search
    redirect_to hourly_productions_path(date: params[:date])
  end
end
