require 'csv'

class UploadController < ApplicationController

  #data is saved if no errors occured
  after_action :create_data, only: [:upload], if: -> { flash[:error].nil? }

  def initialize
    @accept_list = ['.csv']
  end

  def index
  end

  #Upload action
  def upload

    check_file_type
    #if any error occured, redirect to the upload form with an error message
    unless flash[:error].nil?
      redirect_to upload_path and return
    end
    #else, display hourly productions
    redirect_to :controller => 'hourly_productions', :action => 'index'
  end

  private

  #check if file has csv extension
  def check_file_type

    csv_file = params[:csv_file]
    white_list = ['application/vnd.ms-excel', 'text/csv', 'application/octet-stream']
    max_size = 5000;

    unless white_list.include? csv_file.content_type
      flash[:error] = "Wrong Type. CSV Expected."
      return
    end
    unless csv_file.size < max_size
      flash[:error] = "File must be under #{max_size}o."
      return
    end

    #if nothing went wrong, check file content
    check_file_data
  end

  #check file content
  def check_file_data

    csv_file = params[:csv_file]
    headers = []
    #check if file has the right headers
    begin
      headers = CSV.open(csv_file, 'r') do |csv|
        csv.first
      end
    rescue
      send_data_error
      return
    end

    unless headers == ['identifier', 'datetime', 'energy']
      send_data_error
    end

  end

  def send_data_error
    flash[:error] = "Unrecognized data."
    return
  end

  #create data if nothing went wrong
  def create_data

    csv_file = params[:csv_file]
    hourly_productions = []
    inverters = []

    CSV.foreach(csv_file, headers: true) do |row|
      begin
        hash_row = row.to_h
        date = Date.strptime( hash_row['datetime'].split(' ')[0], "%d/%m/%y")
        hourly_production = {date: date , hour: hash_row['datetime'].split(' ')[1], inverter_id: hash_row['identifier'], production: hash_row['energy']}
        #ignore invalid data
        if hash_row['identifier'].to_i == 0
          next
        end
        inverters |= [hash_row['identifier']]
        hourly_productions << hourly_production if hourly_production.values.all?
      rescue
        #if any error, skip to next row
        next
      end
    end

    #build an hash array with inverter's ids
    inverters.map! do |x|
      {id: x}
    end
    #if inverter already in database, ignore
    Inverter.import(inverters, on_duplicate_key_ignore: true)
    #if data already in database, update
    HourlyProduction.import(hourly_productions, on_duplicate_key_update: {constraint_name: 'composite_key', columns: [:production]})
  end
end
