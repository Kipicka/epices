# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_09_19_173501) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "hourly_productions", force: :cascade do |t|
    t.date "date", null: false
    t.time "hour", null: false
    t.bigint "inverter_id", null: false
    t.float "production", default: 0.0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["inverter_id", "date", "hour"], name: "composite_key", unique: true
    t.index ["inverter_id"], name: "index_hourly_productions_on_inverter_id"
  end

  create_table "inverters", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "hourly_productions", "inverters"
end
