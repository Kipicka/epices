class CreateHourlyProductions < ActiveRecord::Migration[6.0]
  def change
    create_table :hourly_productions do |t|
      t.date :date, null: false
      t.time :hour, null: false
      t.references :inverter, null: false, foreign_key: true
      t.float :production, default: 0.0

      t.timestamps
    end
  end
end
