class AddCompositeKeyConstraint < ActiveRecord::Migration[6.0]
  def up
    execute <<-SQL
      ALTER TABLE hourly_productions ADD CONSTRAINT composite_key UNIQUE (inverter_id, date, hour);
    SQL
  end

  def down
    execute <<-SQL
      ALTER TABLE hourly_productions DROP CONSTRAINT composite_key;
    SQL
  end
end
