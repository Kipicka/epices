require 'test_helper'

class UploadControllerTest < ActionController::TestCase

  test "upload action should redirect to initial form if file is not a csv" do
    uploaded_file = fixture_file_upload('files/test_image.jpg', 'application/jpg')
    post :upload, params: {csv_file: uploaded_file}

    assert_redirected_to upload_path

  end

  test "upload action should redirect to initial form if file is too big" do
    uploaded_file = fixture_file_upload('files/test_huge_file.csv', 'text/csv')
    post :upload, params: {csv_file: uploaded_file}

    assert_redirected_to upload_path

  end

  test "upload action should redirect to initial form if unknown headers" do
    uploaded_file = fixture_file_upload('files/test_wrong_headers.csv', 'text/csv')
    post :upload, params: {csv_file: uploaded_file}

    assert_redirected_to upload_path

  end

  test "upload action should redirect to hourly_productions#index if file is compliant" do
    uploaded_file = fixture_file_upload('files/test_compliant.csv', 'text/csv')
    post :upload, params: {csv_file: uploaded_file}

    assert_redirected_to hourly_productions_path
  end

  test "upload action should feed database with inverters if file is compliant" do
    uploaded_file = fixture_file_upload('files/test_compliant.csv', 'text/csv')
    post :upload, params: {csv_file: uploaded_file}

    assert_equal(2, Inverter.all.count)
  end

  test "upload action should feed database with hourly_productions if file is compliant" do
    uploaded_file = fixture_file_upload('files/test_compliant.csv', 'text/csv')
    post :upload, params: {csv_file: uploaded_file}

    assert_equal(48, HourlyProduction.all.count)
  end

  test "upload action should ignore rows with corrupted data" do
    uploaded_file = fixture_file_upload('files/test_corrupted_rows.csv', 'text/csv')
    post :upload, params: {csv_file: uploaded_file}

    assert_equal(43, HourlyProduction.all.count)
  end

  test "upload action should update data is key already in database" do
    uploaded_file = fixture_file_upload('files/test_update_1.csv', 'text/csv')
    post :upload, params: {csv_file: uploaded_file}

    uploaded_file = fixture_file_upload('files/test_update_2.csv', 'text/csv')
    post :upload, params: {csv_file: uploaded_file}

    assert_equal(4, HourlyProduction.all.first[:production])
  end

end
