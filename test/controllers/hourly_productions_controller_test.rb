require 'test_helper'

class HourlyProductionsControllerTest < ActionDispatch::IntegrationTest

  setup do
    uploaded_file = fixture_file_upload('files/test_data.csv', 'text/csv')
    post upload_upload_path, params: {csv_file: uploaded_file}
  end

    test "@dates should contain all different dates in data" do
      get hourly_productions_path

      assert_equal(4, assigns(:dates).size)
    end

    test "@date should contain corresponding param if specified" do
      get hourly_productions_path, params: {:date => '21/04/44'}

      assert_equal('21/04/44', assigns(:date))
    end

    test "@date should contain default date if no param specified" do
      get hourly_productions_path

      assert_equal(assigns(:dates).first, assigns(:date))
    end

    test "@hourly_productions should contain every entries with specified date" do
      get hourly_productions_path, params: {:date => '10/04/17'}

      assert_equal(45, assigns(:hourly_productions).size)
    end

    test "@hourly_productions should contain every entries with default date if no param specified" do
      get hourly_productions_path

      assert_equal(1, assigns(:hourly_productions).size)
    end

    test "@hourly_productions should contain every entries with default date if wrong param specified" do
      get hourly_productions_path, params: {:date => "pizza savoyarde"}

      assert_equal(1, assigns(:hourly_productions).size)
    end

    test "@day_productions should contain sum of production for specified day" do
      get hourly_productions_path, params: {:date => "12/04/17"}

      assert_equal(1337, assigns(:day_production))
    end

    test "@production_by_inverter should contain sum of production for specified day for each inverter" do
      get hourly_productions_path, params: {:date => "14/04/17"}

      assert_equal({1 =>  10.0, 2 => 7.0}, assigns(:production_by_inverter))
    end

    test "search should redirect to index" do
      post hourly_productions_search_path

      assert_redirected_to hourly_productions_path
    end

end
