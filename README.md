# README

## Introduction
Cette application a pour but de stocker et restituer les données provenant d'onduleurs photovoltaïques, le tout pour chaque heure de chaque jour.
Elle a été développée avec Ruby on Rails et utilise une base de données PostgreSQL. Les données initiales proviennent de fichiers au format CSV.

## Mise en place
Voici les différentes étapes nécessaires pour lancer cette application :

* Cloner le repository
* Lancer une base PostgreSQL sur le port 5433 (si une base est déjà existante, modifier le port dans le fichier config/database.yml)
* Lancer les commandes suivantes à la racine du projet:

```bash
bundle update
rails db:create
rails db:migrate
rails s
```

## Fonctionnement de l'application
L'application tourne sur le  localhost:3000. La page d'accueil affiche les productions horaires ainsi qu'un lien "Upload data". Ce lien dirige vers un formulaire permettant d'upload vers le serveur les différents fichiers CSV contenant les données à ajouter.

La structure attendue pour les fichiers CSV est la suivante:

```csv
identifier,datetime,energy
1,11/04/17 00:00,0
2,11/04/17 00:00,0
...
```
Un fichier ne contenant pas cette entête sera refusé.

Lors de l'upload, les données non conformes sont ignorées. Les données déjà présentes dans la base sont mises à jour.

Après l'upload, l'utilisateur est redirgé vers l'affichage des données. Les données sont affichées dans un tableau. Un sélecteur permet de changer la date.
