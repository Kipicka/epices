Rails.application.routes.draw do
  get 'upload', :to => 'upload#index'
  post 'upload/upload'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :hourly_productions, only: [:index]
  post 'hourly_productions/search'
  root to: "hourly_productions#index"
end
